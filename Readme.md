Code for my (yet unpublished) blog post about C++ metaprogramming.

Since I leverage (recent) boost libraries tree contains a copy of boost 1.75.

Python code
-----------

To play with the code you'll need a Python 3.8 intepreter and (optionally)
``poetry`` package managment tool. 

Installation instructions (using poetry)

```
cd python
poetry install 
poetry shell
cd src 
py.test test_code.py # Run tests to see that everything works. 
```

without poetry

```
cd python
# create virtualenv
pip install -r requirements.txt
# activate the virtualenv
cd src 
py.test test_code.py # Run tests to see that everything works. 
```

C++ Code
--------

You'll need C++ compiler that supporting C++x17 standard (tested on GCC).

Build instructions: 

```
cd cpp
mkdir build 
cd build 
cmake ..
make all 
./test # Check code passes sanity checks. 
```