import typing

import attr


@attr.dataclass(eq=True, hash=True)
class OrderItem:
    item_name: str
    quantity: int
    price_cents: int


@attr.dataclass(eq=True, hash=True)
class Order:
    order_no: str
    items: typing.List[OrderItem]
