import typing

import attr
import attr.exceptions


class JsonParseException(Exception):
    pass


def is_list(annotation):
    """
    Returns True if passed annotation is of typing.List
    """
    try:
        return annotation.__origin__ == list
    except AttributeError:
        return False


def get_list_type_param(annotation):
    """
    Extract typing parameter from typing annotation.

    >>> get_list_type_param(typing.List[str]) == str
    True
    """
    return annotation.__args__[0]


def from_json(data: typing.Any, typing_annotation: typing.Any):
    if typing_annotation == int:
        return int(data)
    elif typing_annotation == float:
        return float(data)
    elif typing_annotation == str:
        return str(data)
    elif is_list(typing_annotation):
        return tuple(
            from_json(item, get_list_type_param(typing_annotation)) for item in data
        )
    elif attr.has(typing_annotation):
        converted = {}
        for field in attr.fields(typing_annotation):
            if field.name in data:
                converted[field.name] = from_json(data[field.name], field.type)
        return typing_annotation(**converted)
    else:
        raise JsonParseException(f"Can't handle {typing_annotation}")
