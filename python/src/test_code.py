import pytest

from dto import Order, OrderItem
from format import to_json
from parse import from_json


def test_serialization():
    order = Order(
        order_no="2020/12/13/123",
        items=[
            OrderItem("Lego Set 2134", quantity=1, price_cents=100 * 100),
            OrderItem("Lego Set 321", quantity=3, price_cents=15 * 100),
        ],
    )
    assert to_json(order) == {
        "items": [
            {"item_name": "Lego Set 2134", "price_cents": 10000, "quantity": 1},
            {"item_name": "Lego Set 321", "price_cents": 1500, "quantity": 3},
        ],
        "order_no": "2020/12/13/123",
    }


def test_parsing():
    order = Order(
        order_no="2020/12/13/123",
        items=(
            OrderItem("Lego Set 2134", quantity=1, price_cents=100 * 100),
            OrderItem("Lego Set 321", quantity=3, price_cents=15 * 100),
        ),
    )
    json = {
        "items": [
            {"item_name": "Lego Set 2134", "price_cents": 10000, "quantity": 1},
            {"item_name": "Lego Set 321", "price_cents": 1500, "quantity": 3},
        ],
        "order_no": "2020/12/13/123",
    }

    assert from_json(json, Order) == order
