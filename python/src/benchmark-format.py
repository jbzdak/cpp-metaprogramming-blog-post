import dto, format, json, parse


infile = "/tmp/benchmark-json.json"
repeats = 500 * 1000

count = 0

needle =  dto.Order(
order_no="2020/12/13/3",
items=(
    dto.OrderItem(
        item_name="Forgotten Realms Saga tome 1",
        quantity=1,
        price_cents=15 * 100,
    ),
    dto.OrderItem(
        item_name="Forgotten Realms Saga tome 2",
        quantity=1,
        price_cents=15 * 100,
    ),
    dto.OrderItem(
        item_name="Forgotten Realms Saga tome 3",
        quantity=1,
        price_cents=15 * 100,
    ),
    dto.OrderItem(
        item_name="Forgotten Realms Saga tome 4",
        quantity=1,
        price_cents=20 * 100,
    ),
    dto.OrderItem(
        item_name="Forgotten Realms Saga tome 6",
        quantity=1,
        price_cents=20 * 100,
    ),
    dto.OrderItem(
        item_name="Forgotten Realms Saga tome 7",
        quantity=1,
        price_cents=20 * 100,
    ),
)
)

with open(infile, mode="r") as infile:

    for line in infile:
        order = parse.from_json(json.loads(line), dto.Order)
        if order == needle:
            count += 1

print(count)