import dto, format, json

orders = []

orders.append(
    dto.Order(
        order_no="2020/12/13/1",
        items=(
            dto.OrderItem(item_name="Lego Set 2137", quantity=1, price_cents=100 * 100),
            dto.OrderItem(item_name="Lego Set 321", quantity=3, price_cents=15 * 100),
        ),
    )
)

orders.append(
    dto.Order(
        order_no="2020/12/13/2",
        items=(
            dto.OrderItem(
                item_name="Super duper graphics card",
                quantity=1,
                price_cents=2 * 1000 * 100,
            ),
        ),
    )
)

orders.append(
    dto.Order(
        order_no="2020/12/13/3",
        items=(
            dto.OrderItem(
                item_name="Forgotten Realms Saga tome 1",
                quantity=1,
                price_cents=15 * 100,
            ),
            dto.OrderItem(
                item_name="Forgotten Realms Saga tome 2",
                quantity=1,
                price_cents=15 * 100,
            ),
            dto.OrderItem(
                item_name="Forgotten Realms Saga tome 3",
                quantity=1,
                price_cents=15 * 100,
            ),
            dto.OrderItem(
                item_name="Forgotten Realms Saga tome 4",
                quantity=1,
                price_cents=20 * 100,
            ),
            dto.OrderItem(
                item_name="Forgotten Realms Saga tome 6",
                quantity=1,
                price_cents=20 * 100,
            ),
            dto.OrderItem(
                item_name="Forgotten Realms Saga tome 7",
                quantity=1,
                price_cents=20 * 100,
            ),
        ),
    )
)

orders.append(
    dto.Order(
        order_no="2020/12/13/4",
        items=(
            dto.OrderItem(
                item_name="Fine fountain pen", quantity=1, price_cents=15 * 1000 * 100
            ),
        ),
    )
)

orders.append(
    dto.Order(
        order_no="2020/12/13/4",
        items=(
            dto.OrderItem(
                item_name="Fine fountain pen", quantity=1, price_cents=15 * 1000 * 100
            ),
        ),
    )
)

orders.append(
    dto.Order(
        order_no="2020/12/13/5",
        items=(
            dto.OrderItem(
                item_name="Hand disinfectant gallon", quantity=10, price_cents=15 * 100
            ),
            dto.OrderItem(
                item_name="Face mask 500 pcs", quantity=10, price_cents=25 * 100
            ),
        ),
    )
)

orders.append(
    dto.Order(
        order_no="2020/12/13/6",
        items=(
            dto.OrderItem(item_name="Cat food bulk 5kg", quantity=5, price_cents=799),
            dto.OrderItem(item_name="Cat litter 10kg", quantity=2, price_cents=499),
        ),
    )
)

orders.append(
    dto.Order(
        order_no="2020/12/13/7",
        items=(
            dto.OrderItem(item_name="Tiger onesee", quantity=1, price_cents=50 * 100),
        ),
    )
)

orders.append(
    dto.Order(
        order_no="2020/12/13/8",
        items=(
            dto.OrderItem(
                item_name="Harry Bosh novel 1", quantity=1, price_cents=15 * 100
            ),
            dto.OrderItem(
                item_name="Harry Bosh novel 2", quantity=1, price_cents=15 * 100
            ),
            dto.OrderItem(
                item_name="Harry Bosh novel 3", quantity=1, price_cents=15 * 100
            ),
            dto.OrderItem(
                item_name="Harry Bosh novel 4", quantity=1, price_cents=20 * 100
            ),
            dto.OrderItem(
                item_name="Harry Bosh novel 5", quantity=1, price_cents=20 * 100
            ),
            dto.OrderItem(
                item_name="Harry Bosh novel 6", quantity=1, price_cents=20 * 100
            ),
        ),
    )
)


orders.append(
    dto.Order(
        order_no="2020/12/13/9",
        items=(
            dto.OrderItem(
                item_name="Cyberpunk 2077 Game", quantity=1, price_cents=19999
            ),
            dto.OrderItem(
                item_name="Super duper graphics card",
                quantity=1,
                price_cents=2 * 1000 * 100,
            ),
        ),
    )
)

orders.append(
    dto.Order(
        order_no="2020/12/13/10",
        items=(
            dto.OrderItem(item_name="Fables Comic", quantity=1, price_cents=30 * 100),
        ),
    )
)

outfile = "/tmp/benchmark-json.json"
repeats = 500 * 1000

with open(outfile, mode="w") as out:
    for __ in range(repeats):
        for order in orders:
            out.write(json.dumps(format.to_json(order)))
            out.write("\n")
