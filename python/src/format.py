import attr


def to_json(item) -> dict:
    result = {}
    for field in attr.fields(type(item)):
        value = getattr(item, field.name, None)
        if isinstance(value, (list, tuple)):
            result[field.name] = [to_json(elem) for elem in value]
        elif isinstance(value, dict):
            result[field.name] = {
                field_name: to_json(field_value)
                for (field_name, field_value) in value.values()
            }
        elif attr.has(value):
            value = to_json(value)
            result[field.name] = value
        else:
            result[field.name] = value
    return result
