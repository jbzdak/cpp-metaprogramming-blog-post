
#define BOOST_TEST_MODULE test

#include <boost/test/included/unit_test.hpp>
#include <boost/json/src.hpp>
#include <cctype>
#include <dto.h>
#include <format.h>
#include <parse.h>

// trim from start (in place)
static inline void ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](unsigned char ch) {
        return !std::isspace(ch);
    }));
}

// trim from end (in place)
static inline void rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(), [](unsigned char ch) {
        return !std::isspace(ch);
    }).base(), s.end());
}

std::string trim(std::string s) {
    ltrim(s);
    rtrim(s);
    return s;
}

namespace json = boost::json;
namespace hana = boost::hana;

BOOST_AUTO_TEST_CASE(TestSanity)
{
    BOOST_TEST(1 == 1);

    json::object foo = {
            {"foo", 5}
    };

    json::object bar = {
            {"foo", 5}
    };

    BOOST_TEST(foo == bar);
}


struct PrintProperty {
    std::ostream &out;

    template<typename HanaString, typename T>
    void operator()(HanaString name, T t) {
        out << hana::to<const char *>(name) << " " << t << "\n";
    }
};

template<typename T>
void PrintHanaStruct(T t, std::ostream &out) {
    hana::for_each(t, hana::fuse(PrintProperty{out}));
}

BOOST_AUTO_TEST_CASE(TestIteration) {
    std::stringstream out;

    dto::OrderedItem item = {.item_name="Lego Set 2134", .quantity=1, .price_cents = 100 * 100};

    PrintHanaStruct(item, out);
    BOOST_TEST(trim(out.str()) == trim(R"(
item_name Lego Set 2134
quantity 1
price_cents 10000
)"));
}


BOOST_AUTO_TEST_CASE(TestFormat)
{
    dto::Order order = {
            .order_no = "2020/12/13/123",
            .items = {
                    {.item_name="Lego Set 2134", .quantity=1, .price_cents = 100 * 100},
                    {.item_name="Lego Set 321", .quantity=3, .price_cents = 15 * 100}
            }
    };

    json::value actual = format::FormatObject(order);
    BOOST_ASSERT(actual.kind() == json::kind::object);
    json::object expected, item1, item2;

    item1["item_name"] = "Lego Set 2134";
    item1["quantity"] = 1;
    item1["price_cents"] = 100 * 100;


    item2["item_name"] = "Lego Set 321";
    item2["quantity"] = 3;
    item2["price_cents"] = 15 * 100;

    expected["order_no"] = "2020/12/13/123";
    expected["items"] = {item1, item2};

    BOOST_TEST(actual.as_object() == expected);
}

BOOST_AUTO_TEST_CASE(TestEqual)
{
    BOOST_TEST(!dto::objects_equal(dto::OrderedItem{.item_name = "foo"}, dto::OrderedItem{.item_name = "bar"}));
    BOOST_TEST(dto::objects_equal(dto::OrderedItem{.item_name = "foo"}, dto::OrderedItem{.item_name = "foo"}));
}

BOOST_AUTO_TEST_CASE(TestParse)
{

    json::object input, item1, item2;

    item1["item_name"] = "Lego Set 2134";
    item1["quantity"] = 1;
    item1["price_cents"] = 100 * 100;


    item2["item_name"] = "Lego Set 321";
    item2["quantity"] = 3;
    item2["price_cents"] = 15 * 100;

    input["order_no"] = "2020/12/13/123";
    input["items"] = {item1, item2};

    dto::Order expected = {
            .order_no = "2020/12/13/123",
            .items = {
                    {.item_name="Lego Set 2134", .quantity=1, .price_cents = 100 * 100},
                    {.item_name="Lego Set 321", .quantity=3, .price_cents = 15 * 100}
            }
    };

    auto actual = parse::ParseObject<dto::Order>(input);

    BOOST_TEST(dto::objects_equal(expected, actual));
}

BOOST_AUTO_TEST_CASE(TestParseEdgeCase)
{
    {
        // Structure parsed from empty object is the same as default constructed
        json::object empty_object = {};
        dto::OrderedItem expected = {};

        BOOST_TEST(dto::objects_equal(parse::ParseObject<dto::OrderedItem>(empty_object), expected));

    }
    {
        // Structure parsed from string is the same as default constructed
        json::string string = "foo";
        dto::OrderedItem expected = {};

        BOOST_TEST(dto::objects_equal(parse::ParseObject<dto::OrderedItem>(string), expected));

    }
    {
        // Conversion of 3.5 to int would be lossy, so we ignore this field.
        json::object float_to_int = {
                {"item_name", "Lego Set 2137"},
                {"quantity", 3.5}
        };

        dto::OrderedItem expected = {.item_name = "Lego Set 2137"};

        BOOST_TEST(dto::objects_equal(parse::ParseObject<dto::OrderedItem>(float_to_int), expected));
    }
}