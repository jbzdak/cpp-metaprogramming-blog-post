#include <iostream>
#include <fstream>

#include "vector"
#include "dto.h"
#include "format.h"


int main() {
    std::ios::sync_with_stdio(false);
    constexpr const char* outfile = "/tmp/benchmark-cpp.json";
    constexpr int repeats = 500 * 1000;

    std::vector<dto::Order> orders;
    orders.emplace_back(dto::Order{
            .order_no = "2020/12/13/1",
            .items = {
                    {.item_name="Lego Set 2137", .quantity=1, .price_cents = 100 * 100},
                    {.item_name="Lego Set 321", .quantity=3, .price_cents = 15 * 100}
            }
    });

    orders.emplace_back(dto::Order{
            .order_no = "2020/12/13/2",
            .items = {
                    {.item_name="Super duper graphics card", .quantity=1, .price_cents = 2 * 1000 * 100},
            }
    });

    orders.emplace_back(dto::Order{
            .order_no = "2020/12/13/3",
            .items = {
                    {.item_name="Forgotten Realms Saga tome 1", .quantity=1, .price_cents = 15 * 100},
                    {.item_name="Forgotten Realms Saga tome 2", .quantity=1, .price_cents = 15 * 100},
                    {.item_name="Forgotten Realms Saga tome 3", .quantity=1, .price_cents = 15 * 100},
                    {.item_name="Forgotten Realms Saga tome 4", .quantity=1, .price_cents = 20 * 100},
                    {.item_name="Forgotten Realms Saga tome 6", .quantity=1, .price_cents = 20 * 100},
                    {.item_name="Forgotten Realms Saga tome 7", .quantity=1, .price_cents = 20 * 100},
            }
    });

    orders.emplace_back(dto::Order{
            .order_no = "2020/12/13/4",
            .items = {
                    {.item_name="Fine fountain pen", .quantity=1, .price_cents = 15 * 1000 * 100},
            }
    });

    orders.emplace_back(dto::Order{
            .order_no = "2020/12/13/4",
            .items = {
                    {.item_name="Fine fountain pen", .quantity=1, .price_cents = 15 * 1000 * 100},
            }
    });

    orders.emplace_back(dto::Order{
            .order_no = "2020/12/13/5",
            .items = {
                    {.item_name="Hand disinfectant gallon", .quantity=10, .price_cents = 15 * 100},
                    {.item_name="Face mask 500 pcs", .quantity=10, .price_cents = 25 * 100},
            }
    });

    orders.emplace_back(dto::Order{
            .order_no = "2020/12/13/6",
            .items = {
                    {.item_name="Cat food bulk 5kg", .quantity=5, .price_cents = 799},
                    {.item_name="Cat litter 10kg", .quantity=2, .price_cents = 499},
            }
    });

    orders.emplace_back(dto::Order{
            .order_no = "2020/12/13/7",
            .items = {
                    {.item_name="Tiger onesee", .quantity=1, .price_cents = 50 * 100},
            }
    });

    orders.emplace_back(dto::Order{
            .order_no = "2020/12/13/8",
            .items = {
                    {.item_name="Harry Bosh novel 1", .quantity=1, .price_cents = 15 * 100},
                    {.item_name="Harry Bosh novel 2", .quantity=1, .price_cents = 15 * 100},
                    {.item_name="Harry Bosh novel 3", .quantity=1, .price_cents = 15 * 100},
                    {.item_name="Harry Bosh novel 4", .quantity=1, .price_cents = 20 * 100},
                    {.item_name="Harry Bosh novel 5", .quantity=1, .price_cents = 20 * 100},
                    {.item_name="Harry Bosh novel 6", .quantity=1, .price_cents = 20 * 100},
            }
    });


    orders.emplace_back(dto::Order{
            .order_no = "2020/12/13/9",
            .items = {
                    {.item_name="Cyberpunk 2077 Game", .quantity=1, .price_cents = 19999},
                    {.item_name="Super duper graphics card", .quantity=1, .price_cents = 2 * 1000 * 100},
            }
    });

    orders.emplace_back(dto::Order{
            .order_no = "2020/12/13/10",
            .items = {
                    {.item_name="Fables Comic", .quantity=1, .price_cents = 30 * 100},
            }
    });

    std::fstream out(outfile, std::fstream::binary | std::fstream::out | std::fstream::trunc);

    char buffer[ 8192 ];
    boost::json::serializer ser;

    for (int idx = 0; idx < repeats; idx++){
        for (const dto::Order& order : orders){
            boost::json::value formatted = format::FormatObject(order);
            ser.reset(&formatted);
            out << ser.read( buffer ) << "\n";
        }
    }


    return 0;
}
