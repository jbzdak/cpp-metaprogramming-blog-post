#include <iostream>
#include <fstream>

#include "vector"
#include "dto.h"
#include "parse.h"


int main() {
    std::ios::sync_with_stdio(false);
    constexpr const char* infile = "/tmp/benchmark-cpp.json";

    auto needle = dto::Order{
            .order_no = "2020/12/13/8",
            .items = {
                    {.item_name="Harry Bosh novel 1", .quantity=1, .price_cents = 15 * 100},
                    {.item_name="Harry Bosh novel 2", .quantity=1, .price_cents = 15 * 100},
                    {.item_name="Harry Bosh novel 3", .quantity=1, .price_cents = 15 * 100},
                    {.item_name="Harry Bosh novel 4", .quantity=1, .price_cents = 20 * 100},
                    {.item_name="Harry Bosh novel 5", .quantity=1, .price_cents = 20 * 100},
                    {.item_name="Harry Bosh novel 6", .quantity=1, .price_cents = 20 * 100},
            }
    };

    dto::Order last_read;

    std::fstream insteam(infile, std::fstream::binary | std::fstream::in);

    assert(insteam.is_open());

    std::string line;

    boost::json::parser par;

    size_t num_matching = 0;

    while(getline(insteam, line)){
        par.reset();
        par.write(line);
        parse::ParseObject(par.release(), last_read);
        if (dto::objects_equal(last_read, needle)){
            num_matching += 1;
        }
    }

    std::cout << num_matching << std::endl;


    return 0;
}
