#ifndef CPP_SRC_FORMAT_526A13D285B147689962707EE9CA4AFB_H
#define CPP_SRC_FORMAT_526A13D285B147689962707EE9CA4AFB_H

#include <boost/json/src.hpp>
#include <boost/hana.hpp>

#include <vector>

namespace format {

template<typename T>
boost::json::value FormatObject(const T &t);


namespace internal {

template<typename T>
boost::json::value FormatStructure(const T &t) {
    boost::json::object result;
    boost::hana::for_each(t, boost::hana::fuse([&result](auto name, auto member) {
        result.emplace(boost::hana::to<const char *>(name), FormatObject(member));
    }));
    return result;
}

template<typename T, typename std::enable_if_t<std::is_arithmetic<T>::value, bool> = true>
boost::json::value FormatValue(const T &t) {
    return t;
}

template<typename T>
boost::json::value FormatValue(const std::vector<T> &t) {
    boost::json::array result;
    for (T value: t) {
        result.emplace_back(FormatObject<T>(value));
    }
    return result;
}

template<typename T>
boost::json::value FormatValue(const std::basic_string<T> &t) {
    return boost::json::string(t);
}
}

template<typename T>
boost::json::value FormatObject(const T &t) {
    if constexpr (boost::hana::Struct<T>::value) {
        return internal::FormatStructure(t);
    } else {
        return internal::FormatValue(t);
    }
}

}

#endif //CPP_SRC_FORMAT_526A13D285B147689962707EE9CA4AFB_H
