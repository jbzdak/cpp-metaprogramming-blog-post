#ifndef CPP_SRC_PARSE_B44BDDA0212F452380A899E1CB0206C1_H
#define CPP_SRC_PARSE_B44BDDA0212F452380A899E1CB0206C1_H

#include <string>
#include <stdexcept>
#include <boost/json/src.hpp>
#include <boost/hana.hpp>

namespace parse {

namespace json = boost::json;

template<typename T>
bool ParseObject(const json::value &, T &result);

namespace internal {

bool is_number(const json::value &value) {
    return value.is_int64() || value.is_uint64() || value.if_double();
}

template<typename T>
bool ParseValue(const json::value &value, std::vector<T> &output) {
    if (!value.is_array()) {
        return false;
    }
    output.clear();
    for (const json::value &item : value.as_array()) {
        output.emplace_back();
        ParseObject(item, output.back());
    }
    return true;
}

template<typename T>
bool ParseValue(const json::value &value, std::basic_string<T> &output) {
    if (!value.is_string()) {
        return false;
    }
    output = json::value_to<std::string>(value);
    return true;
}

template<typename T, typename std::enable_if_t<std::is_arithmetic<T>::value, bool> = true>
bool ParseValue(const json::value &value, T &output) {
    if (!is_number(value)) {
        return false;
    }
    try {
        output = json::value_to<T>(value);
    } catch (boost::wrapexcept<boost::system::system_error>&){
        return false;
    }
    return true;
}

template<typename T>
bool ParseStructure(const json::value &value, T &output) {
    if (!value.is_object()) {
        return false;
    }
    const json::object &object = value.as_object();

    boost::hana::for_each(boost::hana::keys(output), [&output, &object](auto key) {
        const auto it = object.find(boost::hana::to<const char *>(key));
        if (it != object.end()) {
            ParseObject(it->value(), boost::hana::at_key(output, key));
        }
    });
    return true;
}

}

template<typename T>
bool ParseObject(const json::value &value, T &result) {
    if constexpr (boost::hana::Struct<T>::value) {
        return internal::ParseStructure(value, result);
    } else {
        return internal::ParseValue(value, result);
    }
}

template<typename T>
T ParseObject(const json::value &value) {
    T result{};
    ParseObject(value, result);
    return result;
}

}

#endif //CPP_SRC_PARSE_B44BDDA0212F452380A899E1CB0206C1_H
