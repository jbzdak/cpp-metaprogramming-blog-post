#ifndef CPP__DTO_C945555EE547424192C42EBF1AB685C5_H
#define CPP__DTO_C945555EE547424192C42EBF1AB685C5_H

#include <cstdint>
#include <vector>

#include <boost/hana.hpp>

namespace dto {

struct OrderedItem {
    BOOST_HANA_DEFINE_STRUCT(
            OrderedItem,
            (std::string, item_name),
            (int64_t, quantity),
            (int64_t, price_cents)
    );
};

struct Order {
    BOOST_HANA_DEFINE_STRUCT(
            Order,
            (std::string, order_no),
            (std::vector<OrderedItem>, items)
    );
};

template<typename T>
bool objects_equal(T left, T right) {

    if constexpr (boost::hana::Struct<T>::value) {
        bool result = true;
        boost::hana::for_each(boost::hana::keys(left), [&result, &left, &right](auto key) {
            if (!objects_equal(boost::hana::at_key(left, key), boost::hana::at_key(right, key))) {
                result = false;
            }
        });
        return result;
    } else {
        return left == right;
    }
}

template<typename T>
bool objects_equal(std::vector<T> left, std::vector<T> right) {
    if (left.size() != right.size()) {
        return false;
    }
    for (size_t index = 0; index < left.size(); index++) {
        if (!objects_equal(left[index], right[index])) {
            return false;
        }
    }
    return true;
}

}

#endif //CPP__DTO_C945555EE547424192C42EBF1AB685C5_H
